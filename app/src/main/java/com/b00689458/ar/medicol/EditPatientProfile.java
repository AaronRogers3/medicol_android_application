package com.b00689458.ar.medicol;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

public class EditPatientProfile extends AppCompatActivity {

    ImageView userPic;
    ImageView coverPhoto;
    ImageView saveData;
    Uri selectedImage;
    String imageSelected;

    EditText editTextUserName;
    EditText DOB;
    EditText editTextHeight;
    EditText editTextWeight;
    EditText editTextAboutMe;

    private int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editpatientprofile);

        userPic = findViewById(R.id.profile);
        coverPhoto = findViewById(R.id.header_cover_image);
        saveData = findViewById(R.id.SaveData);

        userPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 1);
            }
        });

        editTextUserName = findViewById(R.id.name);
        DOB = findViewById(R.id.DateOfBirth);
        editTextHeight = findViewById(R.id.Height);
        editTextWeight = findViewById(R.id.Weight);
        editTextAboutMe = findViewById(R.id.edtInput);


        saveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = editTextUserName.getText().toString();
                String dateOfBirth = DOB.getText().toString();
                String height = editTextHeight.getText().toString();
                String weight = editTextWeight.getText().toString();
                String aboutMe = editTextAboutMe.getText().toString();

                if (userName.equals("")|| dateOfBirth.equals("") || height.equals("") || weight.equals("") || aboutMe.equals("")) {
                    final AlertDialog alertDialog = new AlertDialog.Builder(EditPatientProfile.this).create();
                    alertDialog.setTitle("Data Omission!");
                    alertDialog.setMessage("Not all data has been entered! Please check your entries...");
                    alertDialog.setIcon(R.drawable.alerticon);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    alertDialog.dismiss();
                                }
                            });
                    alertDialog.show();
                } else {
                    imageSelected = selectedImage.toString();
                    SharedPreferences sharedPreferences = getSharedPreferences("MYPREFS", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("userName", userName);
                    editor.putString("DOB", dateOfBirth);
                    editor.putString("height", height);
                    editor.putString("weight", weight);
                    editor.putString("aboutme", aboutMe);
                    editor.putString("image", imageSelected);
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), PatientProfile.class);
                    startActivity(intent);
                }
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 0:
                if(resultCode == RESULT_OK){
                    selectedImage = imageReturnedIntent.getData();
                    userPic.setImageURI(selectedImage);
                }

                break;
            case 1:
                if(resultCode == RESULT_OK){
                    selectedImage = imageReturnedIntent.getData();
                    userPic.setImageURI(selectedImage);
                }
                break;
        }
    }
}