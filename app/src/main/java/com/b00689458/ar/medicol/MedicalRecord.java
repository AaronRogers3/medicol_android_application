package com.b00689458.ar.medicol;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MedicalRecord extends AppCompatActivity {

    String getcon1;
    String getcon2;
    String getcon3;
    String getmed1;
    String getmed2;
    String getmed3;
    String getall1;
    String getall2;
    String getall3;

    TextView tvcon1;
    TextView tvcon2;
    TextView tvcon3;
    TextView tvmed1;
    TextView tvmed2;
    TextView tvmed3;
    TextView tvall1;
    TextView tvall2;
    TextView tvall3;

    Button Home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.medical_record);

        tvcon1 = findViewById(R.id.condition1);
        tvcon2 = findViewById(R.id.condition2);
        tvcon3 = findViewById(R.id.condition3);
        tvmed1 = findViewById(R.id.Medication1);
        tvmed2 = findViewById(R.id.Medication2);
        tvmed3 = findViewById(R.id.Medication3);
        tvall1 = findViewById(R.id.Allergy1);
        tvall2 = findViewById(R.id.Allergy2);
        tvall3 = findViewById(R.id.Allergy3);

        SharedPreferences sharedPreferences = getSharedPreferences("MYPREFS", MODE_PRIVATE);
            getcon1 = sharedPreferences.getString("con1", null);
            getcon2 = sharedPreferences.getString("con2", null);
            getcon3 = sharedPreferences.getString("con3", null);
            getmed1 = sharedPreferences.getString("med1", null);
            getmed2 = sharedPreferences.getString("med2", null);
            getmed3 = sharedPreferences.getString("med3", null);
            getall1 = sharedPreferences.getString("all1", null);
            getall2 = sharedPreferences.getString("all2", null);
            getall3 = sharedPreferences.getString("all3", null);

            if (getcon1 != null) {
                tvcon1.setText(getcon1);
            }
            if (getcon2 != null) {
                tvcon2.setText(getcon2);
            }
            if (getcon3 != null) {
                tvcon3.setText(getcon3);
            }
            if (getmed1 != null) {
                tvmed1.setText(getmed1);
            }
            if (getmed2 != null) {
                tvmed2.setText(getmed2);
            }
            if (getmed3 != null) {
                tvmed3.setText(getmed3);
            }
            if (getall1 != null) {
                tvall1.setText(getall1);
            }
            if (getall2 != null) {
                tvall2.setText(getall2);
            }
            if (getall3 != null) {
                tvall3.setText(getall3);
            }

            Home = findViewById(R.id.home);
            Home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), Dashboard.class);
                    startActivity(intent);
                }
            });
    }
}
