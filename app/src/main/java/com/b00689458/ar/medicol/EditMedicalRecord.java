package com.b00689458.ar.medicol;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;


public class EditMedicalRecord extends AppCompatActivity {

    EditText editTextC1;
    EditText editTextC2;
    EditText editTextC3;
    EditText editTextM1;
    EditText editTextM2;
    EditText editTextM3;
    EditText editTextA1;
    EditText editTextA2;
    EditText editTextA3;

    String con1;
    String con2;
    String con3;
    String med1;
    String med2;
    String med3;
    String all1;
    String all2;
    String all3;

    Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editmedicalrecord);

        editTextC1 = findViewById(R.id.condition1);
        editTextC2 = findViewById(R.id.condition2);
        editTextC3 = findViewById(R.id.condition3);
        editTextM1 = findViewById(R.id.medication1);
        editTextM2 = findViewById(R.id.medication2);
        editTextM3 = findViewById(R.id.medication3);
        editTextA1 = findViewById(R.id.allergy1);
        editTextA2 = findViewById(R.id.allergy2);
        editTextA3 = findViewById(R.id.allergy3);

        save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                con1 = editTextC1.getText().toString();
                con2 = editTextC2.getText().toString();
                con3 = editTextC3.getText().toString();
                med1 = editTextM1.getText().toString();
                med2 = editTextM2.getText().toString();
                med3 = editTextM3.getText().toString();
                all1 = editTextA1.getText().toString();
                all2 = editTextA2.getText().toString();
                all3 = editTextA3.getText().toString();

                if (con1.equals("") || con2.equals("") || con3.equals("") || med1.equals("") || med2.equals("") || med3.equals("") ||
                        all1.equals("") || all2.equals("") || all3.equals("")) {
                    final AlertDialog alertDialog = new AlertDialog.Builder(EditMedicalRecord.this).create();
                    alertDialog.setTitle("Data Omission!");
                    alertDialog.setMessage("Not all data has been entered! Please check your entries...");
                    alertDialog.setIcon(R.drawable.alerticon);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    alertDialog.dismiss();
                                }
                            });

                    alertDialog.show();
                } else {

                    SharedPreferences sharedPreferences = getSharedPreferences("MYPREFS", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("con1", con1);
                    editor.putString("con2", con2);
                    editor.putString("con3", con3);
                    editor.putString("med1", med1);
                    editor.putString("med2", med2);
                    editor.putString("med3", med3);
                    editor.putString("all1", all1);
                    editor.putString("all2", all2);
                    editor.putString("all3", all3);
                    editor.apply();

                    AlertDialog alertDialog = new AlertDialog.Builder(EditMedicalRecord.this).create();
                    alertDialog.setTitle("Sucessfully Recorded!");
                    alertDialog.setMessage("Would you like to view your medical record?");
                    alertDialog.setIcon(R.drawable.greentick);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(getApplicationContext(), MedicalRecord.class);
                                    startActivity(intent);
                                }
                            });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(getApplicationContext(), Dashboard.class);
                                    startActivity(intent);
                                }
                            });
                    alertDialog.show();
                }
            }
        });
    }
}
