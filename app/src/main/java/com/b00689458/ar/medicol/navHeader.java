package com.b00689458.ar.medicol;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class navHeader extends AppCompatActivity {

    ImageView profileicon;
    TextView name;
    String nameretrieved;
    String imageURIRetrived;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_header_navigation);

        profileicon = findViewById(R.id.imageView);
        name = findViewById(R.id.name);

        SharedPreferences sharedPreferences = getSharedPreferences("MYPREFS", MODE_PRIVATE);
        nameretrieved = sharedPreferences.getString("userName", null);
        if (nameretrieved.equals("")) {
            name.setText("MediCol");
        } else {
            name.setText(nameretrieved);
        }

        imageURIRetrived = sharedPreferences.getString("image", null);
        if (imageURIRetrived != null) {
            Uri myUri = Uri.parse(imageURIRetrived);
            profileicon.setImageURI(myUri);
        } else {
            profileicon.setImageResource(R.drawable.medicon);
        }
    }
}