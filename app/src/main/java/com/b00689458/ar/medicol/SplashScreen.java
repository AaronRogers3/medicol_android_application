package com.b00689458.ar.medicol;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashScreen extends AppCompatActivity {

        //time variable
        private final int SPLASH_LENGTH = 3000;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.splashscreen);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent splashIntent = new Intent(getApplicationContext(),
                            Login.class);
                    startActivity(splashIntent);
                    SplashScreen.this.finish();
                }
            }, SPLASH_LENGTH);
        }
    }


