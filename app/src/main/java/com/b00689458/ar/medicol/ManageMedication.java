package com.b00689458.ar.medicol;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class ManageMedication extends AppCompatActivity {

    Button add;
    Button minus;
    int inhalerlevel = 40;
    TextView displaylevel;
    TextView inhalerName;
    String showInhalerLevel;
    Spinner spinnerInhalers;
    ImageView imageView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.managemedication);

        add = findViewById(R.id.buttonadd);
        minus = findViewById(R.id.buttonminus);
        showInhalerLevel = String.valueOf(inhalerlevel);
        spinnerInhalers = findViewById(R.id.spinnerInhalers);
        displaylevel = findViewById(R.id.textViewStockBlue);
        imageView2 = findViewById(R.id.imageView2);
        displaylevel.setText(showInhalerLevel);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inhalerlevel = inhalerlevel + 1;
                int newlevel = inhalerlevel + 1;
                String shownewlevel = String.valueOf(newlevel);
                displaylevel.setText(shownewlevel);
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inhalerlevel = inhalerlevel - 1;
                int newlevel = inhalerlevel - 1;
                String shownewlevel = String.valueOf(newlevel);
                displaylevel.setText(shownewlevel);
            }
        });

        final ArrayAdapter<String> myAdapter2 = new ArrayAdapter<>(ManageMedication.this,
                android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.inhalers));
        myAdapter2.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1);
        spinnerInhalers.setAdapter(myAdapter2);

        spinnerInhalers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position2, long l) {


                switch (position2) {
                    case 0:
                        imageView2.setImageResource(R.drawable.browninhaler);
                        break;
                    case 1:
                        imageView2.setImageResource(R.drawable.redinhaler);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                String[] list = getResources().getStringArray(R.array.inhalers);

                spinnerInhalers.setSelection(Integer.parseInt(list[0]));
            }

        });

    }
}
