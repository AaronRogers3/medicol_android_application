package com.b00689458.ar.medicol;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class PatientProfile extends AppCompatActivity {

    ImageView userPic;
    ImageView coverPhoto;
    ImageView editData;

    TextView TvUserName;
    TextView TvDOB;
    TextView TvHeight;
    TextView TvWeight;
    TextView TvAboutMe;

    Button Home;

    private int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpatientprofile);

        userPic = findViewById(R.id.UserPicture);
        coverPhoto = findViewById(R.id.header_cover_image);
        editData = findViewById(R.id.EditData);

        TvUserName = findViewById(R.id.name);
        TvDOB = findViewById(R.id.DateOfBirth);
        TvHeight = findViewById(R.id.textViewHeight);
        TvWeight = findViewById(R.id.textViewWeight);
        TvAboutMe = findViewById(R.id.textViewAboutMe);

        SharedPreferences sharedPreferences = getSharedPreferences("MYPREFS", MODE_PRIVATE);
        String userName = sharedPreferences.getString("userName", null);
        String dateOfBirth = sharedPreferences.getString("DOB", null);
        String height = sharedPreferences.getString("height", null);
        String weight = sharedPreferences.getString("weight", null);
        String aboutMe = sharedPreferences.getString("aboutme", null);

        String imageURI = sharedPreferences.getString("image", null);
        if (imageURI != null) {
            Uri myUri = Uri.parse(imageURI);
            userPic.setImageURI(myUri);
        }

        TvUserName.setText(userName);
        TvDOB.setText(dateOfBirth);
        TvHeight.setText(height);
        TvWeight.setText(weight + "lbs");
        TvAboutMe.setText(aboutMe);

        editData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EditPatientProfile.class);
                startActivity(intent);
            }
        });

        Home = findViewById(R.id.home);
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Dashboard.class);
                startActivity(intent);
            }
        });
    }
}