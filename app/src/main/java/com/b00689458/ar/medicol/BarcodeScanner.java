package com.b00689458.ar.medicol;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScanner;
import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScannerBuilder;
import com.google.android.gms.vision.barcode.Barcode;

public class BarcodeScanner extends AppCompatActivity {

    TextView textView;
    Button button;
    EditText editText;
    Barcode barcodeResult;
    String result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barcodescanner);

        textView = findViewById(R.id.textViewScan);
        button = findViewById(R.id.buttonScan);
        editText = findViewById(R.id.editTextScan);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startScan();
            }
        });
    }

    private void startScan() {
        final MaterialBarcodeScanner materialBarcodeScanner = new MaterialBarcodeScannerBuilder()
                .withActivity(BarcodeScanner.this)
                .withEnableAutoFocus(true)
                .withBleepEnabled(true)
                .withBackfacingCamera()
                .withCenterTracker()
                .withText("Scanning...")
                .withResultListener(new MaterialBarcodeScanner.OnResultListener() {
                    @Override
                    public void onResult(Barcode barcode) {
                        barcodeResult = barcode;
                        result = barcode.rawValue;
                        editText.setText(result);

                        try {
                            Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                            String term = result + " Allergy Information List";
                            intent.putExtra(SearchManager.QUERY, term);
                            startActivity(intent);
                        } catch (Exception e) {
                            Toast.makeText(BarcodeScanner.this, "No Search", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .build();
        materialBarcodeScanner.startScan();
    }

}
